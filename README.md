<!--required for Modrinth centering -->
<center>
<div style="text-align:center;line-height:250%">

## Mod Id

[![Minecraft versions](https://cf.way2muchnoise.eu/versions/Minecraft_CURSE_ID_all.svg)](https://modrinth.com/mod/mod-id/versions#all-versions)
![environment: client](https://img.shields.io/badge/environment-client-1976d2)
![environment: server](https://img.shields.io/badge/environment-server-orangered)
![environment: server, opt client](https://img.shields.io/badge/environment-server,_opt_client-c65135)
![environment: both](https://img.shields.io/badge/environment-both-4caf50)  
[![loader: Fabric](https://img.shields.io/badge/loader-Fabric-cdc4ae?logo=data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMjYgMjgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoyIj48cGF0aCBkPSJtMTAgMjQtOC04TDE0IDRWMmgybDYgNnY0TDEwIDI0eiIgc3R5bGU9ImZpbGw6I2RiZDBiNCIvPjxwYXRoIGQ9Ik0xMiA2djJoLTJ2Mkg4djJINnYySDR2MkgydjRoMnYyaDJ2MmgydjJoMnYtMmgydi0yaDJ2LTJoMnYtMmgydi0yaDJ2LTRoLTJ2LTJoLTJWOGgtMlY2aDJ2MmgydjJoMnYyaDJ2Mmgydi0yaC0yVjhoLTJWNmgtMlY0aC0yVjJoMnYyaDJ2MmgydjJoMnYyaDJ2NGgtMnYyaC00djJoLTJ2MmgtMnYyaC0ydjRoLTJ2Mkg4di0ySDZ2LTJINHYtMkgydi0ySDB2LTRoMnYtMmgydi0yaDJ2LTJoMlY4aDJWNmgyVjJoMlYwaDJ2MmgtMnY0aC0yIiBzdHlsZT0iZmlsbDojMzgzNDJhIi8+PHBhdGggc3R5bGU9ImZpbGw6IzgwN2E2ZCIgZD0iTTIyIDEyaDJ2MmgtMnoiLz48cGF0aCBkPSJNMiAxOGgydjJoMnYyaDJ2MmgydjJIOHYtMkg2di0ySDR2LTJIMnYtMiIgc3R5bGU9ImZpbGw6IzlhOTI3ZSIvPjxwYXRoIGQ9Ik0yIDE2aDJ2MmgydjJoMnYyaDJ2Mkg4di0ySDZ2LTJINHYtMkgydi0yeiIgc3R5bGU9ImZpbGw6I2FlYTY5NCIvPjxwYXRoIGQ9Ik0yMiAxMnYtMmgtMlY4aC0yVjZsLTIuMDIzLjAyM0wxNiA4aDJ2MmgydjJoMnpNMTAgMjR2LTJoMnYtNGg0di00aDJ2LTJoMnY0aC0ydjJoLTJ2MmgtMnYyaC0ydjJoLTIiIHN0eWxlPSJmaWxsOiNiY2IyOWMiLz48cGF0aCBkPSJNMTQgMThoLTR2LTJIOHYtMmgydjJoNHYyem00LTRoLTR2LTJoLTJ2LTJoLTJWOGgydjJoMnYyaDR2MnpNMTQgNGgydjJoLTJWNHoiIHN0eWxlPSJmaWxsOiNjNmJjYTUiLz48L3N2Zz4=)](https://fabricmc.net/)
<a href="https://quiltmc.org/"><img alt="available for: Quilt Loader" width=73 src="https://raw.githubusercontent.com/QuiltMC/art/master/brand/1024png/quilt_available_dark.png"></a>  
<a href="https://modrinth.com/mod/fabric-api/versions"><img alt="Requires: Fabric API" width="60" src="https://i.imgur.com/Ol1Tcf8.png"></a>
[![supports: Cloth Config](https://img.shields.io/badge/supports-Cloth_Config-89dd43?logo=data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj48c3ZnIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHZpZXdCb3g9IjAgMCAyNjQgMjY0IiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zOnNlcmlmPSJodHRwOi8vd3d3LnNlcmlmLmNvbS8iIHN0eWxlPSJmaWxsLXJ1bGU6ZXZlbm9kZDtjbGlwLXJ1bGU6ZXZlbm9kZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6MjsiPjxwYXRoIGlkPSJiYWNrZ3JvdW5kIiBkPSJNODQsMjA3bC0zNiwtMzZsMCwtMzlsMTgsLTE4bDE4LC0yMWwyMSwtMThsMzYsLTM2bDAsLTIxbDIxLDBsMTgsMjFsMzYsMzZsMCwxOGwtNTQsNzhsLTM5LDM2bC0zOSwwWiIgc3R5bGU9ImZpbGw6IzljZmY1NTsiLz48cGF0aCBpZD0ib3V0bGluZSIgZD0iTTE2MiwxOGwwLC0xOGwtMjEsMGwwLDE4bC0xOCwwbDAsMzlsLTE4LDBsMCwxOGwtMjEsMGwwLDE4bC0xOCwwbDAsMjFsLTE4LDBsMCwxOGwtMTgsMGwwLDE4bC0yMSwwbDAsMzlsMjEsMGwwLDE4bDE4LDBsMCwxOGwxOCwwbDAsMjFsMTgsMGwwLDE4bDIxLDBsMCwtMThsMTgsMGwwLC0yMWwxOCwwbDAsLTE4bDIxLDBsMCwtMThsMTgsMGwwLC0xOGwxOCwwbDAsLTIxbDM5LDBsMCwtMThsMTgsMGwwLC0zOWwtMTgsMGwwLC0xOGwtMjEsMGwwLC0xOGwtMTgsMGwwLC0xOGwtMTgsMGwwLC0yMWwtMTgsMGwwLDIxbDE4LDBsMCwxOGwxOCwwbDAsMThsMTgsMGwwLDM5bDIxLDBsMCwxOGwtMjEsMGwwLC0xOGwtMTgsMGwwLC0yMWwtMTgsMGwwLC0xOGwtMTgsMGwwLC0xOGwtMjEsMGwwLDE4bDIxLDBsMCwxOGwxOCwwbDAsMjFsMTgsMGwwLDM2bC0xOCwwbDAsMjFsLTE4LDBsMCwxOGwtMjEsMGwwLDE4bC0xOCwwbDAsMThsLTE4LDBsMCwyMWwtMjEsMGwwLC0yMWwtMTgsMGwwLC0xOGwtMTgsMGwwLC0xOGwtMTgsMGwwLC0zOWwxOCwwbDAsLTE4bDE4LDBsMCwtMThsMTgsMGwwLC0yMWwyMSwwbDAsLTE4bDE4LDBsMCwtMThsMTgsMGwwLC0zOWwyMSwwIiBzdHlsZT0iZmlsbDojMTE1MTEwOyIvPjxwYXRoIGlkPSJzaGFkZS0xIiBzZXJpZjppZD0ic2hhZGUgMSIgZD0iTTQ4LDE3MWwtMTgsMGwwLDE4bDE4LDBsMCwxOGwxOCwwbDAsMThsMTgsMGwwLDIxbDIxLDBsMCwtMjFsLTIxLDBsMCwtMThsLTE4LDBsMCwtMThsLTE4LDBsMCwtMThabTE2OCwtNTdsMjEsMGwwLDE4bC0yMSwwbDAsLTE4WiIgc3R5bGU9ImZpbGw6IzZlYWEzMzsiLz48cGF0aCBpZD0ic2hhZGUtMiIgc2VyaWY6aWQ9InNoYWRlIDIiIGQ9Ik00OCwxNTBsLTE4LDBsMCwyMWwxOCwwbDAsMThsMTgsMGwwLDE4bDE4LDBsMCwxOGwyMSwwbDAsLTE4bC0yMSwwbDAsLTE4bC0xOCwwbDAsLTE4bC0xOCwwbDAsLTIxWiIgc3R5bGU9ImZpbGw6IzgwY2MzZDsiLz48cGF0aCBpZD0ic2hhZGUtMyIgc2VyaWY6aWQ9InNoYWRlIDMiIGQ9Ik0xMDUsMjI1bDAsLTE4bDE4LDBsMCwtMzZsMzksMGwwLC0zOWwxOCwwbDAsLTE4bDE4LDBsMCwzNmwtMTgsMGwwLDIxbC0xOCwwbDAsMThsLTIxLDBsMCwxOGwtMTgsMGwwLDE4bC0xOCwwWm0xMTEsLTExMWwtMTgsMGwwLC0yMWwtMTgsMGwwLC0xOGwtMTgsMGwwLC0xOGwxOCwwbDAsMThsMTgsMGwwLDE4bDE4LDBsMCwyMVoiIHN0eWxlPSJmaWxsOiM4OWRjNDI7Ii8+PHBhdGggaWQ9InNoYWRlLTQiIHNlcmlmOmlkPSJzaGFkZSA0IiBkPSJNMTQxLDE3MWwtMzYsMGwwLC0yMWwtMjEsMGwwLC0xOGwyMSwwbDAsMThsMzYsMGwwLDIxWm0zOSwtMzlsLTM5LDBsMCwtMThsLTE4LDBsMCwtMjFsLTE4LDBsMCwtMThsMTgsMGwwLDE4bDE4LDBsMCwyMWwzOSwwbDAsMThabS0xOCwtNzVsLTIxLDBsMCwtMThsMjEsMGwwLDE4WiIgc3R5bGU9ImZpbGw6IzkwZWI0YjsiLz48L3N2Zz4=)](https://modrinth.com/mod/cloth-config/versions)
[![supports: Mod Menu](https://img.shields.io/badge/supports-Mod_Menu-134bfe?logo=data:image/webp+xml;base64,UklGRlIAAABXRUJQVlA4TEUAAAAv/8F/AA9wpv9T/M///McDFLeNpKT/pg8WDv6jiej/BAz7v+bbAKDn9D9l4Es/T/9TBr708/Q/ZeBLP0//c7Xiqp7z/QEA)](https://modrinth.com/mod/modmenu/versions)  
[![license: MIT](https://img.shields.io/badge/license-MIT-white)](https://gitlab.com/GITLAB_GROUP/mod_id/-/blob/master/LICENSE)
[![source on: GitLab](https://img.shields.io/badge/source_on-GitLab-fc6e26?logo=gitlab)](https://gitlab.com/GITLAB_GROUP/mod_id)
[![issues: GitLab](https://img.shields.io/gitlab/issues/open-raw/GITLAB_GROUP/mod_id?label=issues&logo=gitlab)](https://gitlab.com/GITLAB_GROUP/mod_id/-/issues)
[![localized: Percentage](https://badges.crowdin.net/mod-id/localized.svg)](https://crwd.in/mod-id)  
[![Modrinth: Downloads](https://img.shields.io/modrinth/dt/mod-id?logo=modrinth&label=Modrinth&color=00ae5d)](https://modrinth.com/mod/mod-id/versions)
[![CurseForge: Downloads](https://img.shields.io/curseforge/dt/CURSE_ID?logo=curseforge&label=CurseForge&color=f16437)](https://www.curseforge.com/minecraft/mc-mods/mod-id/files)  
[![chat: Discord](https://img.shields.io/discord/1006391289006280746?logo=discord&color=5964f3)](https://discord.gg/xABmPngXAH)
<a href="https://coindrop.to/supersaiyansubtlety"><img alt="coindrop.to me" width="82" style="border-radius:3px" src="https://coindrop.to/embed-button.png"></a>
<a href="https://ko-fi.com/supersaiyansubtlety"><img alt="Buy me a coffee" width="106" src="https://i.ibb.co/4gwRR8L/p.png"></a>

</div>
</center>

---

<details>

<summary>About this template</summary>

### This is a template mod.

It's highly personalised to my workflow, and it references my handle (supersaiyansubtlety/sssubtlety) in quite a few places.

Feel free to fork it and create your own template, but remember to replace these references, especially the donation links in this README.md.

To use it, replace all occurrences of Mod Id/mod_id/mod-id in file/directory names and in strings (**except** in `fabric.mod.json`!) with the corresponding forms of your mod's identifier.

`mod_id` and `ModId` in `fabric.mod.json` should be kept as they will be replaced with values exported from `build.gradle`.

#### Important Building Notes

- The build script expects to find `CURSE_API_KEY.secret` and `MODRINTH_TOKEN.secret` at the directory level above the project folder.

  Building will fail unless you create these files or remove/modify the code that references them in `build.gradle`.

- The build script expects the first line of `CHANGELOG.md` to follow the format
  > `- #.#[.#] (## Mth. 20##): //...`

  where '#' represents a digit 0-9.

  You can see/modify the exact regular expression in `build.gradle`.

- If you use the `publishMod` task:
    - First create Modrinth/CurseForge projects and put their project ids in `gradle.properties`
    -  Fill out the `relations` portion of the `curseforge` block with your mod's relations
    - Use the command `gradlew publishMod`
    - A changelog will be taken from the top-most entry in `CHANGELOG.md`
        - You can append previous versions' changes using the command `gradlew publishMod -PnumVersions=#` where `#` is an integer grater than 1 (and not greater than the number of entries in `CHANGELOG.md`)
        - You can print a preview of the changelog to console by running `gradlew printRecentChanges`, or `gradlew printRecentChanges -PnumVersions=#`

#### Features

This template attempts to keep things DRY.

It centralizes as many values as possible into `gradle.properties`, and dynamically transforms the `mod_name` property into other forms (`mod_id` `mod-id` and `ModId`) so you don't have to specify them yourself.

With the exception of the `fabric-loom` plugin, all versions can be changed in `gradle.properties`, without touching `build.gradle`.

You can specify multiple `minecraft_versions` in `gradle.properties` with comma-separated versions (no spaces). It's assumed these are in increasing order.

I've set thing up so `build.gradle` and `fabric.mod.json` rarely need to be touched to update the mod.

It's setup to have Mod Menu and Cloth Config as optional dependencies (it should be simple to swap cloth for your favorite config lib).

It's setup to use CrowdinTranslate to fetch translations from a project on Crowdin at game start, and has an option for users to disable this.

</details>

---

#### Fill in:
Mod Id, mod-id, mod_id  
GITLAB_GROUP -> supersaiyansubtlety | supersaiyansubtlety-group/minecraft-mods  
CURSE_ID  
client-side | server-side | both  
Mod description

---

### Brief summary goes here

Works client-side and in single player.
Works server-side and in single player.
Must be installed on both client and server.

Mod description goes here.

---

<details>

<summary>Configuration</summary>

Supports [Cloth Config](https://modrinth.com/mod/cloth-config/versions) and
[Mod Menu](https://modrinth.com/mod/modmenu/versions) for configuration, but neither is required.

Options will use their default values if [Cloth Config](https://modrinth.com/mod/cloth-config/versions) is absent.

If Cloth Config is present, options can be configured either through
[Mod Menu](https://modrinth.com/mod/modmenu/versions) or by editing `config/mod_id.json` in your
instance folder (`.minecraft/` by default for the vanilla launcher).

- Download translation updates; default: `true`
  > Download translations from Crowdin when the game launches.

</details>

---

<details>

<summary>Translating</summary>

[![localized: Percentage](https://badges.crowdin.net/mod-id/localized.svg)](https://crwd.in/mod-id)  
If you'd like to help translate Mod Id, you can do so on
[Crowdin](https://crwd.in/mod-id).  
New translations will be added once approved without the mod needing an update thanks to
[CrowdinTranslate](https://github.com/gbl/CrowdinTranslate).

</details>

---

This mod is only for Fabric (works on Quilt, too!) and I won't be porting it to Forge. The license is MIT, however,
so anyone else is free to port it.

I'd appreciate links back to this page if you port or otherwise modify this project, but links aren't required.
