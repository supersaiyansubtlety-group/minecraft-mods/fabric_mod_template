- 1.0.3 (20 Jun. 2022):

  Update to 1.19.

  Update dependencies.

  Update mod_gradle_scripts.

  Fix path comment in PreLaunch.

- 1.0.2 (14 Mar. 2022): 
  
  - Updated for 1.18.2
  - Update README with more badges
  - Updated to use mod_gradle_scripts 1.1.2

- 1.0.1 (14 Jan. 2022): Update README with info on how to personalize the template.
- 1.0.0 (14 Jan. 2022):
  - Updated to 1.18.1
  - Reworked most of the template:
    - `CHANGELOG.md` now reflects changes to the template rather than being a placeholder
    - Reworked  `README.md`
    - `Init` and `ClientInit` are inner classes in `ModId`
    - Cloth config is optional
    - Replaced placeholder `booleanConfig` with `fetch_translation_updates` which is checked in `ClientInit` before downloading translations from Crowdin
    - Added `Util` class
    - `"recommends"` minimum versions of `"modmenu"` and `"cloth-config"` in `fabric.mod.json`