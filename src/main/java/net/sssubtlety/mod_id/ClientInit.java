package net.sssubtlety.mod_id;

import de.guntram.mcmod.crowdintranslate.CrowdinTranslate;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

import static net.sssubtlety.mod_id.FeatureControl.shouldFetchTranslationUpdates;

public class ClientInit implements ClientModInitializer {
    @Override
    @Environment(EnvType.CLIENT)
    public void onInitializeClient() {
        if (shouldFetchTranslationUpdates())
            CrowdinTranslate.downloadTranslations("mod-id", ModId.NAMESPACE);
    }
}
