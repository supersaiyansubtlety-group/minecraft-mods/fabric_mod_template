package net.sssubtlety.mod_id;

import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.fabricmc.loader.api.VersionParsingException;
import net.fabricmc.loader.api.metadata.version.VersionPredicate;
import net.minecraft.client.resource.language.I18n;
import net.minecraft.text.Text;
import net.minecraft.text.TextColor;
import net.minecraft.util.Formatting;

import java.util.Optional;

public final class Util {
    private Util() { }

    public static Text replace(Text text, String regex, String replacement) {
        String string = text.getString();
        string = string.replaceAll(regex, replacement);
        return Text.literal(string).setStyle(text.getStyle());
    }

    public static boolean isModLoaded(String id, String versionPredicate) {
        final Optional<ModContainer> modContainer = FabricLoader.getInstance().getModContainer(id);
        if (modContainer.isPresent()) {
            try {
                return VersionPredicate.parse(versionPredicate).test(modContainer.get().getMetadata().getVersion());
            } catch (VersionParsingException e) {
                ModId.LOGGER.error("Couldn't parse version predicate: {}", versionPredicate, e);
            }
        }

        return false;
    }
}
