package net.sssubtlety.mod_id;

import net.minecraft.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ModId {
	public static final String NAMESPACE = "mod_id";
	public static final Text NAME = Text.translatable("text." + NAMESPACE + ".name");
	public static final Logger LOGGER = LogManager.getLogger();
}
